#!/bin/sh

#define parameters which are passed in.
USR=`cat ~/user`
DOMAIN=$1
EMAIL=$2
TARGET_CONF=$3
CERT_PSWD=$4

#define the template.
cat  << EOF
[program:openstar-cert]
user = $USR
environment=HOME="/$USR",USER="$USR"
directory=/$USR/
command=/$USR/get_cert.sh $DOMAIN $EMAIL $TARGET_CONF $CERT_PSWD
autostart=false
stdout_logfile=/$USR/openstar-cert.console.log
stderr_logfile=/$USR/openstar-cert.err.log

[group:ui]
programs=openstar-cert
EOF
