#!/bin/bash


gitlab_token=$1

rm ./sss-poker-ui-1.0-SNAPSHOT.zip

cp project_subfolder ..
cp project_id ..
cp ui_product_name ..
cp git_tag ..
cp entrypoint.sh ..
cp generate_dockerfile.sh ..
cd ..

./download_tagged_ui_release.sh $gitlab_token

./generate_dockerfile.sh > Dockerfile

docker build --no-cache  -t registry.gitlab.com/openstar/openstar-docker/poker .

docker push registry.gitlab.com/openstar/openstar-docker/poker

