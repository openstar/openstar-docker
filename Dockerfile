FROM registry.gitlab.com/openstar/openstar-docker/base-11

ADD node_product_name /root/
ADD user /root/
ADD ui_product_name /root/
ADD is_file_older /root/
ADD get_cert.sh /root/
ADD generate_supervisor_conf.sh /root/
ADD generate_supervisor_ui_conf.sh /root/
ADD generate_supervisor_cert_conf.sh /root/
ADD remove_port_redirect.sh /root/
ADD add_port_redirect.sh /root/
ADD sss-openstar-node-0.4.0-SNAPSHOT.zip /root/
ADD sss.openstar-ui-1.0-SNAPSHOT.zip /root/
RUN cd /root && unzip sss-openstar-node-0.4.0-SNAPSHOT.zip && unzip sss.openstar-ui-1.0-SNAPSHOT.zip
ADD vapid_private.pem /root/sss-openstar-node-0.4.0-SNAPSHOT/conf/
ADD entrypoint.sh /root/
ENTRYPOINT ["/root/entrypoint.sh"]
