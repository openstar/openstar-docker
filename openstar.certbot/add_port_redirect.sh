#!/bin/sh

if [ "$#" -ne 2 ]; then
    echo "Illegal number of parameters - need from port and to port"
    exit 2
fi

echo FROM PORT $1
echo TO PORT $2

sudo iptables -A PREROUTING -t nat -p tcp --dport $1 -j REDIRECT --to-ports $2

sudo iptables-save
