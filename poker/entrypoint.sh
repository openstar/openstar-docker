#!/bin/bash

phrase=$1
supervisord_password=$2
ui_keystore_pass=$3
bridge_host=$4
pokerId=$5
pokerIdPass=$6

ui_conf_folder=/root/sss.openstar-ui-1.0-SNAPSHOT/conf/

if [ -z "$1" ]
then
   echo "CANNOT be empty"
   exit
fi

if [ -z "$2" ]
then
   echo "phrase CANNOT be empty"
   exit
fi

if [ -z "$3" ]
then
   echo "supervisor password CANNOT be empty"
   exit
fi

if [ -z "$4" ]
then
   echo "WARN empty"
fi

if [ -z "$5" ]
then
   echo "WARN  empty"
fi

echo "Using <node password> , <supervisord password>,  bridge host ${bridge_host}"

mkdir -p /root/openstar

CERTFILE=/root/openstar/openstar_social_pkcs.p12

if [ -f $CERTFILE ]; then
   echo "File $CERTFILE exists. using cert and ssl"
else
   sed -i  's/include "usessl.conf"/include "usedummyssl.conf"/g' ${ui_conf_folder}overrides.conf && echo "File $CERTFILE does not exist, not using ssl."
fi

sed -i "s/password = openstar/password = ${supervisord_password}/g" /etc/supervisor/supervisord.conf

/root/generate_supervisor_ui_conf.sh ${ui_keystore_pass} ${bridge_host} ${pokerId} ${pokerIdPass}  > /etc/supervisor/conf.d/ui.conf


/usr/bin/supervisord -n

