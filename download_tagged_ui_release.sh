#!/bin/bash

gitlab_token=$1
product_name=`cat ui_product_name`
git_tag=`cat git_tag`
job_name=deploy_ui
project_id=`cat project_id`
project_subfolder=`cat project_subfolder`

echo "Removing old backup..."
rm -rf ${product_name}..zip.moved

echo "Backup current..."
mv ${product_name}.zip ${product_name}.zip.moved

echo "Wait for job to finish" 

until $(curl --output /dev/null --silent --head -L -H "PRIVATE-TOKEN: ${gitlab_token}" --fail "https://gitlab.com/api/v4/projects/${project_id}/jobs/artifacts/${git_tag}/raw${project_subfolder}/target/${product_name}.zip?job=${job_name}"); do
    printf '.'
    sleep 5
done

echo "Download archive..."
curl --output ${product_name}.zip -L -H "PRIVATE-TOKEN: ${gitlab_token}" "https://gitlab.com/api/v4/projects/${project_id}/jobs/artifacts/${git_tag}/raw${project_subfolder}/target/${product_name}.zip?job=${job_name}"
