#!/bin/bash

SUB_CMD=1
CERTFILE=/root/openstar/openstar_social_pkcs.p12

while [[ "$#" -gt 0 ]]; do
    case $1 in
        -cf|--certfile) CERTFILE="$2"; shift ;;
        -p|--passphrase) PHRASE="$2"; shift ;;
        -d|--domain) DOMAIN="$2"; shift ;;
        -e|--email) ADMIN_EMAIL="$2"; shift ;;
        -f|--from) FROM="$2"; shift ;;
        -t|--to) TO="$2"; shift ;;
        -u|--unredirect) SUB_CMD=2 ;; 
        -r|--redirect) SUB_CMD=3 ;;
        *) echo "Unknown parameter passed: $1" ;;
    esac
    shift
done


case $SUB_CMD in
    1)
      if [ -z "$DOMAIN" ]; then echo "No"; exit; fi
      if [ -z "$ADMIN_EMAIL" ]; then echo "No"; exit; fi
      if [ -z "$CERTFILE" ]; then echo "No"; exit; fi
      if [ -z "$PHRASE" ]; then echo "No"; exit; fi

      echo "Using domain ${DOMAIN}, email ${ADMIN_EMAIL}, certfile ${CERTFILE}"

      if [ -f $CERTFILE ] && [ ! $(/root/is_file_older $CERTFILE 23) ]; then
         echo "Recent file $CERTFILE exists. Will not attempt to create cert"
      else
         /root/get_cert.sh ${DOMAIN} ${ADMIN_EMAIL} ${CERTFILE} ${PHRASE}
      fi
   ;;
    2)
      echo "Remove redirect $FROM $TO"
      if [ -z "$FROM" ]; then echo "No 'from' port set"; exit; fi
      if [ -z "$TO" ]; then echo "No 'to' port set"; exit; fi
      /root/remove_port_redirect.sh $FROM $TO
      ;;

    3) 
      if [ -z "$FROM" ]; then echo "No 'from' port set"; exit; fi
      if [ -z "$TO" ]; then echo "No 'to' port set"; exit; fi
      echo "Add redirect $FROM $TO"
     /root/add_port_redirect.sh $FROM $TO
     ;;  
esac


