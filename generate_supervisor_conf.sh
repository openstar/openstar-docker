#!/bin/sh

#define parameters which are passed in.

NODE=$1
PASS=$2
PROD=`cat ~/node_product_name`
USR=`cat ~/user`

#define the template.
cat  << EOF
[program:openstar-node-$NODE]
user = $USR
environment=HOME="/$USR",USER="$USR"
directory=/$USR/${PROD}/
command=/$USR/${PROD}/bin/sss-openstar-node -DNODE_ID=$NODE -password $PASS -startRpc -startHttp
autostart=true
stdout_logfile=/$USR/openstar-node.console.log
stderr_logfile=/$USR/openstar-node.err.log

[group:nobus]
programs=openstar-node-$NODE
EOF

