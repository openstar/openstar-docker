#!/bin/bash

gitlab_token=$1
product_name=`cat node_product_name`
git_tag=`cat git_tag`
job_name=deploy

echo "Removing old backup..."
rm -rf ${product_name}.zip.moved

echo "Backup current..."
mv ${product_name}.zip ${product_name}.zip.moved

echo "Wait for job to finish"

until $(curl --output /dev/null --silent --head -L -H "PRIVATE-TOKEN: ${gitlab_token}" --fail "https://gitlab.com/api/v4/projects/9985905/jobs/artifacts/${git_tag}/raw/sss.openstar-node/target/universal/${product_name}.zip?job=${job_name}"); do
    printf '.'
    sleep 60
done

echo "Download archive..."
curl --output ${product_name}.zip -L -H "PRIVATE-TOKEN: ${gitlab_token}" "https://gitlab.com/api/v4/projects/9985905/jobs/artifacts/${git_tag}/raw/sss.openstar-node/target/universal/${product_name}.zip?job=${job_name}"

