#!/bin/sh

if [ "$#" -ne 4 ]; then
    echo "Illegal number of parameters - domain, emailaddr, targetfolder, cert password"
    exit 2
fi
#
# REMEMBER IF PORT 80 IS REDIREcTED USING IPTABLES THIS WONT WORK
#
#

#define parameters which are passed in.
DOMAIN=$1
EMAIL=$2
TARGET_CONF=$3
CERT_PSWD=$4

#define the template.
/usr/local/bin/certbot-auto certonly --standalone --preferred-challenges http -d $DOMAIN --non-interactive --agree-tos -m $EMAIL

CERT_FOLDER=/etc/letsencrypt/live/$DOMAIN/

sudo openssl pkcs12 -export -in ${CERT_FOLDER}fullchain.pem -inkey ${CERT_FOLDER}privkey.pem -out ${TARGET_CONF} -name $DOMAIN -password pass:$CERT_PSWD
