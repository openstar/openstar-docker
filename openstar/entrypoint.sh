#!/bin/bash

node_name=$1
phrase=$2
supervisord_password=$3
ui_keystore_pass=$4

ui_conf_folder=/root/sss.openstar-ui-1.0-SNAPSHOT/conf/

if [ -z "$1" ]
then
   echo "node name CANNOT be empty"
   exit
fi

if [ -z "$2" ]
then
   echo "phrase CANNOT be empty"
   exit
fi

if [ -z "$3" ]
then
   echo "supervisor password CANNOT be empty"
   exit
fi

if [ -z "$4" ]
then
   echo "keystore password CANNOT be empty"
   exit
fi

echo "Using node name ${node_name}, <node password> , <supervisord password>"

mkdir -p /root/openstar

CERTFILE=/root/openstar/openstar_social_pkcs.p12

if [ -f $CERTFILE ]; then
   echo "File $CERTFILE exists. using cert and ssl"
else
   sed -i  's/include "usessl.conf"/include "usedummyssl.conf"/g' ${ui_conf_folder}overrides.conf && echo "File $CERTFILE does not exist, not using ssl."
fi

sed -i "s/password = openstar/password = ${supervisord_password}/g" /etc/supervisor/supervisord.conf

/root/generate_supervisor_conf.sh ${node_name} ${phrase} ${ui_keystore_pass} > /etc/supervisor/conf.d/${node_name}.conf

/root/generate_supervisor_ui_conf.sh ${ui_keystore_pass}  > /etc/supervisor/conf.d/ui.conf

/usr/bin/supervisord -n

