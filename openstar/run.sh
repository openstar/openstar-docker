#!/bin/bash

if [ -z "$1" ]
then
   echo "GITLAB token CANNOT be empty"
   exit
fi


gitlab_token=$1


cp project_subfolder ..
cp project_id ..
cp ui_product_name ..
cp git_tag ..
cp entrypoint.sh ..
cp generate_dockerfile.sh ..
cd ..

./download_tagged_node_release.sh $gitlab_token

./download_tagged_ui_release.sh $gitlab_token

./generate_dockerfile.sh > Dockerfile

docker build --no-cache  -t registry.gitlab.com/openstar/openstar-docker/alpha4 .

docker push registry.gitlab.com/openstar/openstar-docker/alpha4

