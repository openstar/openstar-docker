# dockers

Openstar Docker container for public use. 

# Install 
To run do something like ... 

```
sudo docker pull registry.gitlab.com/openstar/openstar-docker/alpha4

NODE_NAME=<name of node e.g. mars, lowercase, underscore, no numbers>
NODE_PASSWORD=<8 chars plus>
DOCKER_INSTANCE_NAME=<e.g. my_node>
SUPERVISORD_WEB_PASSWORD=<secret for connection to supervisord>
DOMAIN_NAME=<domain must resolve to this IP, or leave out>	
DOMAIN_EMAIL=<a real email addr to satisify letsencrypt, or leave out>
KEY_STORE_PASSWORD=<a password that protects the certificate keystore, or leave out>


sudo docker run \
   --name ${DOCKER_INSTANCE_NAME} \
   --mount type=bind,source=/home/ubuntu/openstar,target=/root/openstar \
   --mount type=bind,source=/dev/urandom,target=/dev/urandom \
   --mount type=bind,source=/dev/random,target=/dev/random \
   --network=host -d registry.gitlab.com/openstar/openstar-docker/alpha4 \
   $NODE_NAME \
   $NODE_PASSWORD \
   $SUPERVISORD_WEB_PASSWORD \
   $DOMAIN_NAME \
   $DOMAIN_EMAIL \
   $KEY_STORE_PASSWORD

```

# Use port 443 for HTTPS

At this point if you've installed a certificate port 8444 is speaking https. 

To direct 443 traffic to that port use the following (outside the container)

```
sudo iptables -A PREROUTING -t nat -p tcp --dport 443 -j REDIRECT --to-ports 8444
sudo iptables-save
```

If you don't use a domain it will operate over http port 8081 for test purposes. 

# Supervisord

After it starts you can examine the processes using supervisord at 

`http://<IP>:9001/`

