#!/bin/sh

#define parameters which are passed in.

domain=$1
PROD=`cat node_product_name`
UI_PROD=`cat ui_product_name`

#define the template.
cat  << EOF
FROM openstar/base:latest

ADD node_product_name /root/
ADD user /root/
ADD ui_product_name /root/
ADD is_file_older /root/
ADD add_port_redirect.sh /root/
ADD remove_port_redirect.sh /root/ 
ADD get_cert.sh /root/
ADD generate_supervisor_conf.sh /root/
ADD generate_supervisor_ui_conf.sh /root/
ADD generate_supervisor_cert_conf.sh /root/
ADD ${PROD}.zip /root/
ADD ${UI_PROD}.zip /root/
RUN cd /root && unzip ${PROD}.zip && unzip ${UI_PROD}.zip
ADD vapid_private.pem /root/${PROD}/conf/
ADD entrypoint.sh /root/
ENTRYPOINT ["/root/entrypoint.sh"]
EOF

