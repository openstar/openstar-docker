#!/bin/sh

#define parameters which are passed in.
PROD=`cat ~/ui_product_name`
USR=`cat ~/user`

#define the template.
cat  << EOF
[program:openstar-ui]
user = $USR
environment=HOME="/$USR",USER="$USR"
directory=/$USR/${PROD}/
command=/$USR/${PROD}/bin/openstar-ui.sh $@
autostart=true
stdout_logfile=/$USR/openstar-ui.console.log
stderr_logfile=/$USR/openstar-ui.err.log

[group:ui]
programs=openstar-ui
EOF
